# RmCopies

RmCopies is a program to delete duplicate files from a folder.


## Acknowledgements

This software uses the following programs as its part:
1) FFmpeg libraries: avcodec, avformat, avutil, swresample, swscale. For the formats and codecs, separate licences apply, please refer to the "ffmpeg" folder in the source code for detailed license information. Parts have been borrowed from the tutorial on [unick-soft.ru](unick-soft.ru) and the official FFmpeg documentation and examples. Please refer to [https://www.ffmpeg.org/doxygen/3.1/modules.html](https://www.ffmpeg.org/doxygen/3.1/modules.html) and [https://github.com/FFmpeg/FFmpeg/tree/master/doc/examples](https://github.com/FFmpeg/FFmpeg/tree/master/doc/examples) for further information.
2) SVD: For SVD, this program uses the implementation ((c) Dianne Cook, All Rights Reserved) from [http://www.public.iastate.edu/~dicook/JSS/paper/code/](http://www.public.iastate.edu/~dicook/JSS/paper/code/), which itself is an adapted version of the algorithm in the "Numerical Recipes in C" book.
