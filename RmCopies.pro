#-------------------------------------------------
#
# Project created by QtCreator 2017-03-20T16:19:26
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RmCopies
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++14

RC_ICONS += rmcopies.png

SOURCES += main.cpp\
    mainwindow.cpp \
    form.cpp \
    mediacompare.cpp \
    mfcc.cpp \
    avdecoder.cpp \
    advancedopts.cpp

HEADERS  += mainwindow.h \
    form.h \
    mediacompare.h \
    internaldefs.h \
    mfcc.h \
    avdecoder.h \
    advancedopts.h

FORMS    += mainwindow.ui \
    form.ui \
    advancedopts.ui

INCLUDEPATH += $$PWD/eigen

RESOURCES += \
    rmcopies.qrc

unix|win32: LIBS += -lavcodec

unix|win32: LIBS += -lavformat

unix|win32: LIBS += -lavutil

unix|win32: LIBS += -lswresample

unix|win32: LIBS += -lswscale
