#include "mediacompare.h"
#include "internaldefs.h"
#include "avdecoder.h"
#include "mfcc.h"

#include <vector>
#include <algorithm>
#include <complex>
#include <eigen/Core>
#include <eigen/SVD>
#include <eigen/CXX11/Tensor>

#include <QImage>
#include <QBitArray>

#include <QDebug>

// Anonymous name space for some sort of simple abstraction
namespace
{

enum RC_Rotation
{
    CW_90 = 0,          // Clock-wise 90 deg
    CCW_90 = 1,         // Counter-Clock-wise 90 deg
    CW_180 = 2,         // 180 deg
    vert_flip = 3,      // Vertical flip
    no_rot = 4,         // No rotation - default
    invalid_rot = 5
};

template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
T median(QVector<T> inVec)
{
    std::sort(inVec.begin(), inVec.end());
    if(inVec.size()%2)
        return inVec[ ( (inVec.size()-1) / 2 ) ];
    else
        return static_cast<T>( ((inVec[((inVec.size())/2)]) + (inVec[((inVec.size()-2)/2)])) / 2 );
}

template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
T getEuclideanDist(QVector<T> a, QVector<T> b)
{
    T res = static_cast<T>(0.0);

    int len = std::min(a.size(), b.size());
    for(int i = 0; i < len; ++i)
        res += ( (a[i] - b[i]) * (a[i] - b[i]) );

    return static_cast<T>( sqrt(res) );
}

void rotateTensor(Eigen::Tensor<uint8_t, 2>& t, RC_Rotation rot = no_rot)
{
    Eigen::array<int, 2> shuff;
    shuff[0] = 1;
    shuff[1] = 0;
    Eigen::array<bool, 2> rev1;
    rev1[0] = true;
    rev1[1] = false;
    Eigen::array<bool, 2> rev2;
    rev2[0] = false;
    rev2[1] = true;
    Eigen::array<bool, 2> rev3;
    rev3[0] = true;
    rev3[1] = true;

    switch(rot)
    {
    case CW_90:
        t = t.shuffle(shuff).reverse(rev1);
        break;
    case CCW_90:
        t = t.shuffle(shuff).reverse(rev1);
        break;
    case CW_180:
        t = t.reverse(rev3);
        break;
    case vert_flip:
        t = t.reverse(rev1);
        break;
    default:
        break;
    }
}

// Digest for audio vectors
QVector<float> getDigestAudio(const QVector<int16_t> samples, AudioParams audParams)
{
    QVector<float> hash(0);

    // Get MFCCs
    MFCC mfccGetter(audParams);
    Eigen::MatrixXf mfcc = mfccGetter.getMfcc(samples);

    int numIter = CASTINT(mfcc.rows()/audParams.FRAME_STEP) - 1;
    hash.resize(numIter*2);
    // SVD of MFCCs FRAME_STEP frames at a time
    for(int i = 0; i < numIter; ++i)
    {
        Eigen::JacobiSVD<Eigen::MatrixXf> svd( mfcc.block( (i*audParams.FRAME_STEP), 0,
                                                           audParams.FRAME_STEP, audParams.NUM_CEPSTRA ) );
        const Eigen::ArrayXf res = svd.singularValues().array();
        hash[i*2]       = res(0);
        hash[(i*2) + 1] = res(1);
    }
    return hash;
}

// Digest for color images
void getDigestColor(QImage * img, QBitArray * hash, float * img_median, ImgParams imgParams)
{
    int nBlocksInRow = imgParams.IMG_SIZE / imgParams.IMG_BLOCK_SIZE;
    int nPixelsInBlock = imgParams.IMG_BLOCK_SIZE * imgParams.IMG_BLOCK_SIZE;
    int sz = nBlocksInRow*nBlocksInRow;
    if( hash->size() != (3*sz) )
        hash->resize(3*sz);
    hash->fill(false);

    if( img->size() != QSize(imgParams.IMG_SIZE,imgParams.IMG_SIZE) )
        return;

    QVector<float> rmeans(sz, 0.0);
    QVector<float> gmeans(sz, 0.0);
    QVector<float> bmeans(sz, 0.0);

    unsigned char * imData = img->bits();

    for(int i = 0; i < imgParams.IMG_SIZE; ++i)
    {
        int idx1 = CASTINT( std::floor(i/imgParams.IMG_BLOCK_SIZE) );
        for( int j = 0; j < imgParams.IMG_SIZE; ++j )
        {
            int idx2 = CASTINT( std::floor(j/imgParams.IMG_BLOCK_SIZE) );
            rmeans[(idx1 * nBlocksInRow) + idx2] += CASTREAL(*imData)/nPixelsInBlock;
            gmeans[(idx1 * nBlocksInRow) + idx2] += CASTREAL(*imData + 1)/nPixelsInBlock;
            bmeans[(idx1 * nBlocksInRow) + idx2] += CASTREAL(*imData + 2)/nPixelsInBlock;
            imData += 3;
        }
    }
    float rmedian, gmedian, bmedian;
    rmedian = median(rmeans);
    gmedian = median(gmeans);
    bmedian = median(bmeans);

    *img_median = (rmedian + gmedian+ bmedian)/3.0f;

    for(int i = 0; i < sz; ++i)
    {
        if(rmeans[i] >= rmedian)
            hash->setBit(i);
        if(gmeans[i] >= gmedian)
            hash->setBit(imgParams.IMG_SIZE + i);
        if(bmeans[i] >= bmedian)
            hash->setBit((imgParams.IMG_SIZE*2) + i);
    }
}

// Digest for grayscale images
void getDigestGray(QImage * img, QBitArray * hash, float * img_median, ImgParams imgParams)
{
    int nBlocksInRow = imgParams.IMG_SIZE / imgParams.IMG_BLOCK_SIZE;
    int nPixelsInBlock = imgParams.IMG_BLOCK_SIZE * imgParams.IMG_BLOCK_SIZE;
    int sz = nBlocksInRow*nBlocksInRow;
    if(hash->size() != sz)
        hash->resize(sz);
    hash->fill(false);

    if(img->size() != QSize(imgParams.IMG_SIZE,imgParams.IMG_SIZE))
        return;

    QVector<float> means(sz, 0.0);

    unsigned char * imData = img->bits();

    for(int i = 0; i < imgParams.IMG_SIZE; ++i)
    {
        int idx1 = CASTINT( std::floor(i/imgParams.IMG_BLOCK_SIZE) );
        for (int j = 0; j < imgParams.IMG_SIZE; ++j)
        {
            int idx2 = CASTINT( std::floor(j/imgParams.IMG_BLOCK_SIZE) );
            means[(idx1 * nBlocksInRow) + idx2] += CASTREAL(*imData)/nPixelsInBlock;
            imData++;
        }
    }
    *img_median = median(means);

    for(int i = 0; i < sz; ++i)
    {
        if(means[i] >= *img_median)
            hash->setBit(i);
    }
}

int getNChannels(QImage * img)
{
    if(img->isGrayscale())
        return 1;

    QImage::Format format = img->format();
    QVector<QImage::Format> rgb;
    rgb<<QImage::Format_RGB32<<QImage::Format_ARGB32<<QImage::Format_ARGB32_Premultiplied<<
         QImage::Format_RGB666<<QImage::Format_ARGB6666_Premultiplied<<QImage::Format_RGB555<<
         QImage::Format_ARGB8555_Premultiplied<<QImage::Format_RGB888<<QImage::Format_RGB444<<
         QImage::Format_ARGB4444_Premultiplied<<QImage::Format_RGBX8888<<QImage::Format_RGBA8888<<
         QImage::Format_BGR30<<QImage::Format_RGBA8888_Premultiplied<<QImage::Format_RGB30<<
         QImage::Format_A2BGR30_Premultiplied<<QImage::Format_A2RGB30_Premultiplied<<QImage::Format_Indexed8;
    QVector<QImage::Format> gray;
    gray<<QImage::Format_Grayscale8;
    QVector<QImage::Format> mono;
    mono<<QImage::Format_Mono<<QImage::Format_MonoLSB;

    if(rgb.indexOf(format) >= 0)
        return 3;
    else if(gray.indexOf(format) >= 0)
        return 1;
    else if(mono.indexOf(format) >= 0)
        return 0;

    return -1;
}

inline float getMSE(Eigen::Tensor<uint8_t, 2>& img1, Eigen::Tensor<uint8_t, 2>& img2)
{
    Eigen::Tensor<float, 0> mse = (img1.cast<float>() - img2.cast<float>()).square().sum();
    return ( std::sqrt(*(mse.data())) / img1.dimension(0) );
}

RC_Rotation getRotation(Eigen::Tensor<uint8_t, 2>& img1, Eigen::Tensor<uint8_t, 2>& img2)
{
    Eigen::ArrayXf rots = Eigen::ArrayXf::Zero(5);
    rots(0) = getMSE(img1, img2);
    Eigen::Tensor<uint8_t, 2> rotcw90 = img2;
    rotateTensor(rotcw90, CW_90);
    rots(1) = getMSE(img1, rotcw90);
    Eigen::Tensor<uint8_t, 2> rotccw90 = img2;
    rotateTensor(rotccw90, CCW_90);
    rots(2) = getMSE(img1, rotccw90);
    Eigen::Tensor<uint8_t, 2> rotcw180 = img2;
    rotateTensor(rotcw180, CW_180);
    rots(3) = getMSE(img1, rotcw180);
    Eigen::Tensor<uint8_t, 2> vf = img2;
    rotateTensor(vf, vert_flip);
    rots(4) = getMSE(img1, vf);

    uint8_t loc = 0;
    rots.minCoeff(&loc);
    if(loc < invalid_rot)
        return RC_Rotation(loc);
    return no_rot;
}

void getFFT(Eigen::Tensor<float, 3>& tensor)
{
    Eigen::array<int, 3> dims = {{0,1,2}};
    Eigen::Tensor<std::complex<float>, 3> out = tensor.template fft<Eigen::BothParts, Eigen::FFT_FORWARD>(dims);
    tensor = out.unaryExpr( [](std::complex<float> a){return std::abs(a);} );
}

} // End anonymous namespace

MediaCompare::MediaCompare():m_isCancelled(false)
{
    m_imgHash.clear();
    m_vidHash.clear();
    m_audHash.clear();
}

MediaCompare::~MediaCompare()
{

}

void MediaCompare::on_cancel()
{
    m_isCancelled = true;
}

void MediaCompare::setImgParams(ImgParams params)
{
    m_imgParams = params;
}

bool MediaCompare::compareImgs(QString imgF1, QString imgF2)
{
    int w1, h1, nChannels1;
    int w2, h2, nChannels2;
    QImage im1(imgF1);
    QImage im2(imgF2);
    w1 = im1.width();
    w2 = im2.width();
    h1 = im1.height();
    h2 = im2.height();

    nChannels1 = getNChannels(&im1);
    nChannels2 = getNChannels(&im2);

    // If one or more images failed to load, or their sizes are too small to be an image,
    // or their sizes are more than a factor of 5 apart, or they are small and are heavily
    // scaled, then do nothing
    if( (im1.isNull()) || (im2.isNull()) || (std::min( std::min(w1,w2), std::min(h1,h2) ) < m_imgParams.IMSIZE_MIN ) ||
        (CASTREAL(w1)/w2 >= m_imgParams.IMSIZE_MAX_SCALING) || (CASTREAL(w2)/w1 >= m_imgParams.IMSIZE_MAX_SCALING)   ||
        ( (nChannels1 < 0) || (nChannels2 < 0) || (nChannels1 != nChannels2) )                 )
    {
        return false;
    }

    QImage tmp1;
    QImage tmp2;

    float median1 = 0.0f;
    float median2 = 0.0f;

    QTransform rot;
    rot.rotate(90);

    if(nChannels1 == 3)
    {
        tmp1 = im1.convertToFormat(QImage::Format_RGB888).scaled( m_imgParams.IMG_SIZE, m_imgParams.IMG_SIZE,
                                                                  Qt::IgnoreAspectRatio,
                                                                  Qt::SmoothTransformation );
        tmp2 = im2.convertToFormat(QImage::Format_RGB888).scaled( m_imgParams.IMG_SIZE, m_imgParams.IMG_SIZE,
                                                                  Qt::IgnoreAspectRatio,
                                                                  Qt::SmoothTransformation );

        int sz = (m_imgParams.IMG_SIZE/m_imgParams.IMG_BLOCK_SIZE)*
                 (m_imgParams.IMG_SIZE/m_imgParams.IMG_BLOCK_SIZE);
        QBitArray hash1(3*sz);
        QBitArray hash2(3*sz);

        getDigestColor(&tmp1, &hash1, &median1, m_imgParams);
        getDigestColor(&tmp2, &hash2, &median2, m_imgParams);

        // If the medians are too far away, the images are not similar
        if( std::abs(median1 - median2) >= m_imgParams.IMG_MED_THRESHOLD )
            return false;

        // Comparing original images
        if((hash1^hash2).count(true) <= m_imgParams.IMG_THRESHOLD)
            return true;

        // 90 degree rotate one image and test again if not equal
        tmp2 = tmp2.transformed(rot, Qt::SmoothTransformation);
        getDigestColor(&tmp2, &hash2, &median2, m_imgParams);
        if((hash1^hash2).count(true) <= m_imgParams.IMG_THRESHOLD)
            return true;

        // 180 degree rotate and test
        tmp2 = tmp2.transformed(rot, Qt::SmoothTransformation);
        getDigestColor(&tmp2, &hash2, &median2, m_imgParams);
        if((hash1^hash2).count(true) <= m_imgParams.IMG_THRESHOLD)
            return true;

        // 270 degree rotate and test
        tmp2 = tmp2.transformed(rot, Qt::SmoothTransformation);
        getDigestColor(&tmp2, &hash2, &median2, m_imgParams);
        if((hash1^hash2).count(true) <= m_imgParams.IMG_THRESHOLD)
            return true;
    } // Endif channels = 3||4
    else if( (nChannels1 == 1) || (nChannels1 == 0) )
    {
        tmp1 = im1.convertToFormat(QImage::Format_Grayscale8).scaled( m_imgParams.IMG_SIZE, m_imgParams.IMG_SIZE,
                                                                      Qt::IgnoreAspectRatio,
                                                                      Qt::SmoothTransformation );
        tmp2 = im2.convertToFormat(QImage::Format_Grayscale8).scaled( m_imgParams.IMG_SIZE, m_imgParams.IMG_SIZE,
                                                                      Qt::IgnoreAspectRatio,
                                                                      Qt::SmoothTransformation );

        QBitArray hash1(m_imgParams.IMG_SIZE);
        QBitArray hash2(m_imgParams.IMG_SIZE);

        getDigestGray(&tmp1, &hash1, &median1, m_imgParams);
        getDigestGray(&tmp2, &hash2, &median2, m_imgParams);

        // If the medians are too far away, the images are not similar
        if( std::abs(median1 - median2) >= m_imgParams.IMG_MED_THRESHOLD )
            return false;

        // Comparing original images
        if((hash1^hash2).count(true) <= m_imgParams.IMG_THRESHOLD)
            return true;

        // 90 degree rotate one image and test again if not equal
        tmp2 = tmp2.transformed(rot, Qt::SmoothTransformation);
        getDigestGray(&tmp2, &hash2, &median2, m_imgParams);
        if((hash1^hash2).count(true) <= m_imgParams.IMG_THRESHOLD)
            return true;

        // 180 degree rotate and test
        tmp2 = tmp2.transformed(rot, Qt::SmoothTransformation);
        getDigestGray(&tmp2, &hash2, &median2, m_imgParams);
        if((hash1^hash2).count(true) <= m_imgParams.IMG_THRESHOLD)
            return true;

        // 270 degree rotate and test
        tmp2 = tmp2.transformed(rot, Qt::SmoothTransformation);
        getDigestGray(&tmp2, &hash2, &median2, m_imgParams);
        if((hash1^hash2).count(true) <= m_imgParams.IMG_THRESHOLD)
            return true;
    } // Endif channels = 1||0

    return false;
}

void MediaCompare::setVideoParams(VideoParams params)
{
    m_vidParams = params;
}

bool MediaCompare::compareVids(QString vidF1, QString vidF2)
{
    qDebug()<<"Comparing: "<<vidF1<<" and "<<vidF2;

    RCVideoDecoder decoder1(vidF1, m_vidParams);
    RCVideoDecoder decoder2(vidF2, m_vidParams);

    VideoInfo info1 = decoder1.getVideoInfo();
    VideoInfo info2 = decoder2.getVideoInfo();

    uint64_t nFrames = CASTUINT64( std::min( (info1.duration * info1.framerate),
                                          (info2.duration * info2.framerate) ) );

    // Return false if the streams aren't valid, if the durations are very different,
    // or if they are too short to compare meaningfully
    if( !(info1.valid) || !(info2.valid) || (nFrames < CASTUINT64(3*m_vidParams.IMG_SIZE)) ||
        (std::abs(info1.duration - info2.duration) >= m_vidParams.MAX_DURATION_DIFF) )
        return false;

    QBitArray digest1( m_vidParams.IMG_SIZE*m_vidParams.IMG_SIZE*m_vidParams.IMG_SIZE );
    QBitArray digest2( m_vidParams.IMG_SIZE*m_vidParams.IMG_SIZE*m_vidParams.IMG_SIZE );

    Eigen::Tensor<uint8_t, 3> frames( m_vidParams.IMG_SIZE, m_vidParams.IMG_SIZE, m_vidParams.IMG_SIZE );
    Eigen::Tensor<uint8_t, 2> currFrame( m_vidParams.IMG_SIZE, m_vidParams.IMG_SIZE );
    Eigen::Tensor<uint8_t, 2> tmpFrame( m_vidParams.IMG_SIZE, m_vidParams.IMG_SIZE );

    decoder1.getNextFrame(currFrame);
    decoder2.getNextFrame(tmpFrame);

    RC_Rotation rot = getRotation(currFrame, tmpFrame);

    uint64_t step = CASTUINT64( std::floor(nFrames/CASTUINT64(m_vidParams.IMG_SIZE)) - 1 );

    // Hash video 1
    uint64_t frameCnt = 0;
    while( frameCnt < CASTUINT64(m_vidParams.IMG_SIZE) )
    {
        for(uint64_t i = 0; i < step; ++i)
            decoder1.skipNextFrame();

        decoder1.getNextFrame(currFrame);
        frames.chip(CASTINT64(frameCnt), 2) = currFrame;

        ++frameCnt;
    }
    Eigen::Tensor<float, 3> hash1 = frames.cast<float>();
    getFFT( hash1 );

    // Hash video 2
    frameCnt = 0;
    while( frameCnt < CASTUINT64(m_vidParams.IMG_SIZE) )
    {
        for(uint64_t i = 0; i < step; ++i)
            decoder2.skipNextFrame();

        decoder2.getNextFrame(currFrame);
        rotateTensor(currFrame, rot); // Only 2 is to be rotated
        frames.chip(CASTINT64(frameCnt), 2) = currFrame;

        ++frameCnt;
    }
    Eigen::Tensor<float, 3> hash2 = frames.cast<float>();
    getFFT( hash2 );

    hash1 -= hash2;
    Eigen::Tensor<float, 0> tmp = hash1.square().mean();

    if( *(tmp.data()) <= m_vidParams.THRESHOLD )
        return true;
    else
        return false;
}

void MediaCompare::setAudioParams(AudioParams params)
{
    m_audParams = params;
}

bool MediaCompare::compareSnds(QString sndF1, QString sndF2)
{
    RCAudioDecoder decoder1(sndF1, m_audParams);
    RCAudioDecoder decoder2(sndF2, m_audParams);

    qDebug()<<"Comparing "<<sndF1<<" and "<<sndF2;

    AudioInfo info1 = decoder1.getAudioInfo();
    AudioInfo info2 = decoder2.getAudioInfo();

    qDebug()<<"  Durations: "<<info1.duration<<" and "<<info2.duration;

    if( !(info1.valid) || !(info2.valid) ||
        (std::abs(info1.duration - info2.duration) >= (info1.duration*m_audParams.MAX_DURATION_DIFF)) )
        return false;

    QVector<int16_t> samples1;
    decoder1.decodeAudio( samples1 );
    if( !samples1.size() )
        return false;
    QVector<float> hash1( getDigestAudio( samples1, m_audParams ) );
    samples1.clear();

    QVector<int16_t> samples2;
    decoder2.decodeAudio( samples2 );
    if( !samples2.size() )
        return false;
    QVector<float> hash2( getDigestAudio( samples2, m_audParams ) );

    if( getEuclideanDist<float>(hash1, hash2) <= m_audParams.THRESHOLD )
        return true;

    return false;
}
