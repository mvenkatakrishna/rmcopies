#ifndef WORKER_H
#define WORKER_H

#include <memory>

#include <QObject>
#include <QFile>
#include <QWidget>

#include "mediacompare.h"

namespace Ui {
class Worker;
}

enum WorkerMessage
{
    MSG_MAX_FSIZE_4GB,      // Max file size is set above 8GB, we will only do binary comparison of files upto 4GB.
    MSG_MAX_FSIZE_1GB,      // Max file size is set above 1GB, we will only do binary comparison.
    MSG_UNSUPPORTED_OS,     // Will perform only binary comparison since media comparison is not supported in this OS.
    MSG_ERR_INCORRECT_PATH, // The root directory is empty or invalid.
    MSG_DRYRUN_FAILED,    // DryRun failed, no write permission?
    MSG_DELETING_FAILED,    // Deleting failed, will try a dry run.
    MSG_INVALID             // Invalid msg from here on.
};

/*
 * Main worker class, which actually does the comparison etc. and also makes the file listing etc. as a supporting activity.
 */
class Worker : public QWidget
{
    Q_OBJECT
public:
    explicit Worker(QWidget *parent = nullptr);

    ~Worker() override;

    /*
     * Funtion to set the parameters for the comparisons.
     */
    void setParams(bool* boList, QStringList* strList, qint64* intList);

    /*
     * Main function to compare files etc.
     */
    void doCompare();

private slots:
    void on_cancelBtn_clicked();

private:
    /*
     * Compares the two files binarily
     */
    bool _binaryCompare(QString file1, QString file2);

    /*
     * Makes a list of files based o nthe internal variables.
     */
    void _makeFileList();

    /*
     * Function to actually perfrom the removal.
     */
    void _doRemoval();

    void _showWarning(WorkerMessage msg);

    bool _showSureDlg();

    virtual void closeEvent(QCloseEvent * e) override;

    Ui::Worker * ui;

    bool _isDryRun;       // When true, the duplicates ar eonly listed out in a text file and not actually deleted.
    bool _isRecursive;    // When true, performs recursive search i nthe directory.
    bool _isKeepNew;      // When true, keeps the newer file and deletes the older.
    bool _isImage;        // When true, looks only for image files and compares only them.
    bool _isVideo;        // When true, looks only for video files and compares only them.
    bool _isAudio;        // When true, looks only for audio files and compares only them.
    bool _isAbsDelete;     // When true, perfroms only absolute binary comparisons.
    bool _isHash;         // When true, compare sonly hash values. Most of the time, good for text and binary files.

    bool _cancelExec;     // When true, the loops for comparison exits.

    QString _rDir;        // The root directory.
    QString _type;        // File extension filter.
    QString _trashDir;    // Trash directory to be used to move the files if not to be deleted.

    qint64 _minSize;         // Minimum file size to be considered.
    qint64 _maxSize;         // Maximum file size to be considered.

    QStringList _fList;    // List of files
    QStringList _fRemList; // List of files to remove
    QStringList _hashList; // List of hash values

    QStringList _mimeTypeList;

    std::unique_ptr<MediaCompare> _mediaCompare;

    VideoParams _videoParams;
    AudioParams _audioParams;
    ImgParams   _imgParams;
};

#endif // WORKER_H
