#ifndef ADVANCEDOPTS_H
#define ADVANCEDOPTS_H

#include <QWidget>
#include <QTabBar>

#include "internaldefs.h"

namespace Ui {
class AdvancedOpts;
}

class AdvancedOpts : public QWidget
{
    Q_OBJECT

public:
    explicit AdvancedOpts(QWidget *parent = nullptr);
    ~AdvancedOpts();

    void getOptions( ImgParams & imgOpts );
    void getOptions( VideoParams & videoOpts );
    void getOptions( AudioParams & audioOpts );

signals:
    void advancedOptionsSet();

private slots:
    void on_cancelBtn_clicked();

    void on_doneBtn_clicked();

    void on_resetBtn_clicked();

    void on_img_thrSlider_sliderMoved(int position);

    void on_img_mthrSlider_sliderMoved(int position);

    void on_img_resetBtn_clicked();

    void on_vid_thrSlider_sliderMoved(int position);

    void on_vid_diffSlider_sliderMoved(int position);

    void on_vid_resetBtn_clicked();

    void on_aud_resetBtn_clicked();

    void on_aud_diffSlider_sliderMoved(int position);

    void on_aud_thrSlider_sliderMoved(int position);

private:
    Ui::AdvancedOpts *ui;

    QTabBar * m_tabBar;

    ImgParams m_imgOpts;
    VideoParams m_videoOpts;
    AudioParams m_audioOpts;
};

#endif // ADVANCEDOPTS_H
