#include <QVector>

#include "avdecoder.h"

extern "C"
{
#include <libavutil/imgutils.h>
}

// ########################################## Audio Decoder ####################################################
RCAudioDecoder::RCAudioDecoder(QString fName, AudioParams params):m_fName(fName),
                                                                  m_info({false, 0}),
                                                                  m_params(params)
{
    av_register_all();

    // get format from audio file
    m_formatCtx = avformat_alloc_context();
    if (avformat_open_input(&m_formatCtx, m_fName.toStdString().c_str(), nullptr, nullptr) != 0)
        return;
    if (avformat_find_stream_info(m_formatCtx, nullptr) < 0)
        return;

    // Find the index of the first audio stream
    int stream_index = -1;
    for (unsigned int i=0; i<m_formatCtx->nb_streams; i++)
    {
        if (m_formatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            stream_index = CASTINT(i);
            break;
        }
    }
    if (stream_index == -1)
        return;

    AVStream* stream = m_formatCtx->streams[stream_index];

    // Set the duration in the info struct
    m_info.duration = CASTREAL(m_formatCtx->duration)/1000000.0f;
    if( m_info.duration <= 0.001f )
        return;

    // For some unknown reason, channel layout mechanism fails for some formats.
    // In this situation, we manually assign some channel layout compatible with the
    // number of channels.
    if( !stream->codecpar->channel_layout )
    {
        switch(stream->codecpar->channels)
        {
        case 1:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_MONO;
            break;
        case 2:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_STEREO;
            break;
        case 3:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_SURROUND;
            break;
        case 4:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_4POINT0;
            break;
        case 5:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_5POINT0;
            break;
        case 6:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_6POINT0;
            break;
        case 7:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_7POINT0;
            break;
        case 8:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_OCTAGONAL;
            break;
        case 16:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_HEXAGONAL;
            break;
        default:
            stream->codecpar->channel_layout = AV_CH_LAYOUT_STEREO_DOWNMIX;
            break;
        }
    }

    // find & open codec
    m_codec = avcodec_find_decoder( stream->codecpar->codec_id );
    m_codecCtx = avcodec_alloc_context3( m_codec );
    if (avcodec_open2(m_codecCtx, m_codec, nullptr) < 0)
        return;
    // Transfer stream parameters to codec context
    if( avcodec_parameters_to_context(m_codecCtx, stream->codecpar) < 0 )
            return;

    // prepare resampler
    m_in_sample_rate = stream->codecpar->sample_rate;
    m_swr = swr_alloc_set_opts(nullptr, AV_CH_LAYOUT_MONO, AV_SAMPLE_FMT_S16, m_params.SAMPLE_RATE,
                       static_cast<int64_t>(stream->codecpar->channel_layout), m_codecCtx->sample_fmt,
                       CASTINT(m_in_sample_rate), 0, nullptr);
    swr_init(m_swr);
    if (!swr_is_initialized(m_swr))
        return;

    // prepare to read data
    //AVPacket packet;
    //av_init_packet(&packet);
    m_frame = av_frame_alloc();
    if (!m_frame)
        return;

    m_info.valid = true;
}

RCAudioDecoder::~RCAudioDecoder()
{
    av_frame_free(&m_frame);
    swr_free(&m_swr);
    avformat_close_input(&m_formatCtx);
    avcodec_free_context(&m_codecCtx);
    avformat_free_context(m_formatCtx);
}

AudioInfo RCAudioDecoder::getAudioInfo()
{
    return m_info;
}

bool RCAudioDecoder::decodeAudio(QVector<int16_t>& samples)
{
    AVPacket packet;
    av_init_packet(&packet);
    while (av_read_frame(m_formatCtx, &packet) >= 0)
    {
        // decode one frame
        int ret = avcodec_send_packet(m_codecCtx, &packet);
        if(ret < 0)
            break;

        while( ret >= 0)
        {
            ret = avcodec_receive_frame(m_codecCtx, m_frame);
            if( (ret == AVERROR(EAGAIN)) && (ret != AVERROR_EOF) )
                break;

            // resample frames
            int16_t* buffer;
            int64_t out_samples = av_rescale_rnd(swr_get_delay(m_swr, m_in_sample_rate) +
                                                   m_frame->nb_samples, m_params.SAMPLE_RATE,
                                             m_in_sample_rate, AV_ROUND_UP);
            av_samples_alloc(reinterpret_cast<uint8_t**>(&buffer), nullptr, 1,
                             CASTINT(out_samples), AV_SAMPLE_FMT_S16, 0);
            int n = swr_convert(m_swr, reinterpret_cast<uint8_t**>(&buffer), m_frame->nb_samples,
                              const_cast<const uint8_t**>(m_frame->data), m_frame->nb_samples);
            // append resampled frames to data vector
            auto it = samples.size();
            samples.resize(samples.size() + n);
            for( ; it != samples.size(); ++it )
            {
                samples[it] = *buffer;
                ++buffer;
            }

            av_freep(buffer);
            av_packet_unref(&packet);
            av_frame_unref(m_frame);
        }
        if( (ret < 0) && (ret != AVERROR(EAGAIN)) && (ret != AVERROR_EOF) )
            break;
    }
    return true;
}

// ########################################## Video Decoder ####################################################
RCVideoDecoder::RCVideoDecoder(QString fName, VideoParams params): m_fName(fName),
                                                                   m_info({false, 0, 0, 0.0f, 0.0f}),
                                                                   m_params(params)
{
    av_register_all();

    // get format from video file
    m_formatCtx = avformat_alloc_context();
    if (avformat_open_input(&m_formatCtx, m_fName.toStdString().c_str(), nullptr, nullptr) != 0)
        return;
    if (avformat_find_stream_info(m_formatCtx, nullptr) < 0)
        return;

    // Find the index of the first video stream
    int stream_index = -1;
    for (unsigned int i=0; i<m_formatCtx->nb_streams; i++)
    {
        if (m_formatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            stream_index = CASTINT(i);
            break;
        }
    }
    if (stream_index == -1)
        return;

    AVStream* stream = m_formatCtx->streams[stream_index];

    // find & open codec
    m_codec = avcodec_find_decoder( stream->codecpar->codec_id );
    m_codecCtx = avcodec_alloc_context3( m_codec );
    if (avcodec_open2(m_codecCtx, m_codec, nullptr) < 0)
        return;
    // Transfer stream parameters to codec context
    if( avcodec_parameters_to_context(m_codecCtx, stream->codecpar) < 0 )
            return;

    // Set the duration in the info struct
    m_info.duration = CASTREAL(m_formatCtx->duration)/1000000.0f;
    if( m_info.duration <= 0.001f )
        return;

    m_info.framerate = ( m_codecCtx->framerate.den ? (m_codecCtx->framerate.num/m_codecCtx->framerate.den) : 0 );
    m_info.width = m_codecCtx->width;
    m_info.height = m_codecCtx->height;

    // Init swscale context
    m_swsCtx = sws_getContext( m_codecCtx->width, m_codecCtx->height, m_codecCtx->pix_fmt,
                           m_params.IMG_SIZE, m_params.IMG_SIZE, AV_PIX_FMT_GRAY8,
                           SWS_FAST_BILINEAR, nullptr, nullptr, nullptr );

    // prepare to read data
    AVPacket packet;
    av_init_packet(&packet);
    m_frame = av_frame_alloc();
    if (!m_frame)
        return;

    m_info.valid = true;
}

RCVideoDecoder::~RCVideoDecoder()
{
    av_frame_free(&m_frame);
    sws_freeContext(m_swsCtx);
    avformat_close_input(&m_formatCtx);
    avcodec_free_context(&m_codecCtx);
    avformat_free_context(m_formatCtx);
}

void RCVideoDecoder::skipNextFrame()
{
    while( !m_frameBuf.size() )
        m_decodeNextPacket();

    m_frameBuf.pop_front();
}

void RCVideoDecoder::getNextFrame(Eigen::Tensor<uint8_t, 2>& img)
{
    while( !m_frameBuf.size() )
        m_decodeNextPacket();

    if( !m_frameBuf.size() )
        img.setZero();
    else
    {
        img = std::move( m_frameBuf[0] );
        m_frameBuf.pop_front();
    }
}

VideoInfo RCVideoDecoder::getVideoInfo()
{
    return m_info;
}

void RCVideoDecoder::m_decodeNextPacket()
{
    AVPacket packet;
    av_init_packet(&packet);
    while (av_read_frame(m_formatCtx, &packet) >= 0)
    {
        // decode one frame
        int ret = avcodec_send_packet(m_codecCtx, &packet);
        if(ret < 0)
            break;

        while( ret >= 0)
        {
            ret = avcodec_receive_frame(m_codecCtx, m_frame);
            if( (ret == AVERROR(EAGAIN)) && (ret != AVERROR_EOF) )
                break;

            // Scale frames
            uint8_t * dst_data[4];
            int dst_linesize[4];
            av_image_alloc(dst_data, dst_linesize, m_params.IMG_SIZE, m_params.IMG_SIZE, AV_PIX_FMT_GRAY8, 1);
            sws_scale(m_swsCtx, m_frame->data, m_frame->linesize, 0, m_frame->height, dst_data, dst_linesize);

            Eigen::Tensor<uint8_t, 2> img(m_params.IMG_SIZE, m_params.IMG_SIZE);
            uint8_t * data = img.data();
            for (int i = 0; i < img.size(); ++i)
                data[i] = dst_data[0][i];
            m_frameBuf.push_back(img);

            av_freep(&dst_data[0]);
            av_packet_unref(&packet);
            av_frame_unref(m_frame);
        }
        if( (ret < 0) && (ret != AVERROR(EAGAIN)) && (ret != AVERROR_EOF) )
            break;
    }
    return;
}
