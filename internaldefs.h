#ifndef INTERNALDEFS_H
#define INTERNALDEFS_H

#include <QBitArray>
#include <QVector>

// ######## General Defs ########################
// Some macros for static casts to make them simpler similar to old C-Style ones
#define CASTUCHAR(x) static_cast<unsigned char>(x)
#define CASTUCHARP(x) static_cast<unsigned char*>(x)
#define CASTINT(x) static_cast<int>(x)
#define CASTUINT(x) static_cast<unsigned int>(x)
#define CASTINT64(x) static_cast<int64_t>(x)
#define CASTUINT64(x) static_cast<uint64_t>(x)
#define CASTREAL(x) static_cast<float>(x)
#define CASTDOUBLE(x) static_cast<double>(x)
#define CASTVOIDP(x) static_cast<void *>(x)

// A small float value for comaprison
#define EPSILON 0.00001f
// A large value implying some sort of infinity
#define LARGE 10000
// ##############################################

// ######## Image Comparison params #############
struct ImgParams
{
    // Image dimensions. Image is assumed to be resized to a square image..
    int   IMG_SIZE           = 256;
    float IMG_THRESHOLD      = 6.0f;
    // Median threshold: since our algorithm is invariant w.r.t. median value,
    // we see if the images have widely differring medians.
    float IMG_MED_THRESHOLD  = 6.0f;
    int   IMSIZE_MIN         = 20;
    float IMSIZE_MAX_SCALING = 8;
    // Size of blocks for mean calculation
    int   IMG_BLOCK_SIZE     = 16;
};
// ##############################################

// ######## Audio Comparison params #############
struct AudioParams
{
    // Maximum percentage difference in video/audio durations (in seconds) that we consider as
    // still being "reasonable"
    float    MAX_DURATION_DIFF = 0.02f;
    // Threshold of difference between the two audio hashes
    float    THRESHOLD         = 10.0f;
    // We consider ferequencies only up to 8kHz
    uint32_t MAX_FREQ          = 8000;
    uint64_t SAMPLE_RATE       = 16000;
    uint32_t MIN_FREQ          = 50;
    // Number of frames in a group for MFCC calculations
    uint8_t  FRAMES_IN_GROUP   = 25;
    // Define values for the filters, frames etc. for the MFCC calculations
    uint8_t  NUM_CEPSTRA       = 24;
    uint32_t FRAME_LEN         = 512;
    uint32_t FRAME_SHIFT       = (FRAME_LEN / 2);
    uint8_t  NUM_FILTERS       = 40;
    uint32_t NUM_FFT_BINS      = (FRAME_LEN / 2) + 1;
    // Number of steps in terms of frames to bunch together when taking SVD of MFCC features
    int      FRAME_STEP        = 5;
    float    PRE_EMPH          = 0.97f;
};
// ##############################################

// ######## Video Comparison params #############
struct VideoParams
{
    // Frame dimensions. Each frame will be resized to this size on both directions.
    // This is also used for the depth in the hash calculations.
    int IMG_SIZE    = 256;
    float THRESHOLD = 6.0f;
    // Maximum percentage difference in video/audio durations (in seconds) that we consider as
    // still being "reasonable"
    float    MAX_DURATION_DIFF = 0.02f;
};

// ##############################################

// Useful structures for various purposes.
// General info regarding video file
struct VideoInfo
{
    bool valid;
    int height;
    int width;
    float framerate;
    float duration;
};

// General info regarding audio file
struct AudioInfo
{
    bool valid;
    float duration;
};

struct ImgHashEntry
{
    QBitArray hash_NoRot;
    QBitArray hash_CW90;
    QBitArray hash_CCW90;
    QBitArray hash_180;
    QBitArray hash_vFlip;
};

struct AudioHashEntry
{
    QVector<float> hash;
};

struct VideoHashEntry
{
    QBitArray hash_NoRot;
    QBitArray hash_CW90;
    QBitArray hash_CCW90;
    QBitArray hash_180;
    QBitArray hash_vFlip;
};

#endif // INTERNALDEFS_H
