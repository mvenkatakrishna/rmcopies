#ifndef MEDIACOMPARE_H
#define MEDIACOMPARE_H

#include <QObject>
#include <QString>
#include <QHash>

#include "internaldefs.h"

class MediaCompare: public QObject
{
    Q_OBJECT
public:
    MediaCompare();
    virtual ~MediaCompare();

    void setImgParams(ImgParams params);
    bool compareImgs(QString imgF1, QString imgF2);
    void setVideoParams(VideoParams params);
    bool compareVids(QString vidF1, QString vidF2);
    void setAudioParams(AudioParams params);
    bool compareSnds(QString sndF1, QString sndF2);

public slots:
    void on_cancel();

private:
    ImgParams   m_imgParams;
    VideoParams m_vidParams;
    AudioParams m_audParams;

    QHash<QString, ImgHashEntry>   m_imgHash;
    QHash<QString, VideoHashEntry> m_vidHash;
    QHash<QString, AudioHashEntry> m_audHash;

    bool m_isCancelled;
};

#endif // MEDIACOMPARE_H

