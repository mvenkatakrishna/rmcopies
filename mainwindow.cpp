#include <cstring>
#include <cmath>
#include <QMessageBox>
#include <QFileDialog>
#include <QProgressDialog>
#include <QStandardPaths>
#include <QDirIterator>
#include <QIcon>
#include <QMimeDatabase>
#include <QDateTime>
#include <QRandomGenerator>
#include <QCryptographicHash>
#include <QThread>
#include <QPalette>

#include <QDebug>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "form.h"

namespace
{

const int BUFFERSIZE = 4096;
const float MEGA = 1048576.0f;

QString getPlatform()
{
#ifdef Q_OS_WIN
    return "Windows";
#endif
#ifdef Q_OS_MAC
    return "MacOS";
#endif
#ifdef Q_OS_LINUX
    return "Linux";
#endif
    return "Unknown";
}

QString getRandomString(uint8_t randomStringLength)
{
   const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    "abcdefghijklmnopqrstuvwxyz"
                                    "0123456789");

   QString randomString = "";
   QRandomGenerator gen( CASTUINT(QDateTime::currentSecsSinceEpoch()) );
   for(uint8_t i=0; i<randomStringLength; ++i)
       randomString.append(possibleCharacters[gen.bounded(possibleCharacters.length())]);
   return randomString;
}

} // End of anonymous namespace


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);

    //QIcon ico("icon.png");
    this->setWindowIcon(QIcon("icon.ico"));

    QPalette paletteB = m_ui->progressTxt->viewport()->palette();
    paletteB.setBrush( m_ui->progressTxt->viewport()->backgroundRole(), QBrush(QColor(50,50,50)) );
    m_ui->progressTxt->viewport()->setPalette(paletteB);

    QPalette paletteF = m_ui->progressTxt->viewport()->palette();
    paletteF.setBrush( m_ui->progressTxt->viewport()->foregroundRole(), QBrush(QColor(200,200,200)) );
    m_ui->progressTxt->viewport()->setPalette(paletteF);

    m_appendProgressString("Welcome!<br>Ready to go!");

    m_myHelpForm = new Form();

    m_advOpts = new AdvancedOpts();

    connect( m_advOpts, SIGNAL(advancedOptionsSet()), this, SLOT(handleAdvancedOptionsSet()) );

    on_resetButton_clicked();

    m_ui->cancelBtn->setDisabled( true );

    // Image types supported
    m_mimeTypeList<<"image/bmp"<<"image/gif"<<"image/jp2"<<"image/jpeg"<<"image/png"<<"image/tiff"<<"image/x-tga"<<
                   "image/x-win-bitmap"<<"image/x-xbitmap";
    // Video types supported
    m_mimeTypeList<<"video/3gpp"<<"video/3gpp2"<<"video/h261"<<"video/h263"<<"video/h264"<<"video/jpeg"<<
                   "video/jpm"<<"video/mp4"<<"video/mpeg"<<"video/ogg"<<"video/quicktime"<<"video/webm"<<
                   "video/x-f4v"<<"video/x-fli"<<"video/x-flv"<<"video/x-m4v"<<"video/x-matroska"<<
                   "video/x-ms-asf"<<"video/x-ms-wm"<<"video/x-ms-wmv"<<"video/x-ms-wmx";
    // Audio types supported
    m_mimeTypeList<<"audio/aac"<<"audio/ac3"<<"audio/flac"<<"audio/mp2"<<"audio/mpeg"<<"audio/x-mpeg"<<
                   "audio/mpeg3"<<"audio/x-mpeg-3"<<"audio/mp4"<<"audio/wav"<<"audio/x-ms-wma"<<"audio/x-wav"<<
                   "audio/x-wavpack"<<"audio/ogg"<<"audio/midi"<<"audio/x-midi"<<"audio/x-raw"<<"audio/aiff"<<
                   "audio/x-aiff"<<"audio/vnd.rn-realaudio";

    m_mediaCompare = std::make_unique<MediaCompare>();
}

MainWindow::~MainWindow()
{
    delete m_ui;
    delete m_myHelpForm;
    delete m_advOpts;
}

void MainWindow::on_helpButton_clicked()
{
    m_myHelpForm->show();
}

void MainWindow::on_quitButton_clicked()
{
    m_myHelpForm->close();
    this->close();
}

void MainWindow::on_resetButton_clicked()
{
    Options tmp;
    m_setFields(tmp);

    m_cancelExec = true;
}

void MainWindow::on_mainButton_clicked()
{
    m_parseOptions();

    m_ui->cancelBtn->setEnabled(true);
    m_ui->mainButton->setDisabled(true);
    m_ui->resetButton->setDisabled(true);

    m_cancelExec = false;
    m_doCompare();
    m_cancelExec = true;

    m_ui->cancelBtn->setDisabled(true);
    m_ui->mainButton->setEnabled(true);
    m_ui->resetButton->setEnabled(true);
}

void MainWindow::on_pathButton_clicked()
{
    m_opts.rDir = QFileDialog::getExistingDirectory( this, tr("Path to the files"),
                                                    QStandardPaths::standardLocations(QStandardPaths::HomeLocation).last(),
                                                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks  );
    m_ui->pathEdit->setText(m_opts.rDir);
}

void MainWindow::on_trashButton_clicked()
{
    m_opts.trashDir = QFileDialog::getExistingDirectory( this, tr("Path to the trash folder"),
                                                        QStandardPaths::standardLocations(QStandardPaths::TempLocation).last(),
                                                        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks  );
    m_ui->trashEdit->setText(m_opts.trashDir);
}

void MainWindow::on_cancelBtn_clicked()
{
    if( m_showSureDlg() )
    {
        m_cancelExec = true;
        m_ui->progressBar->reset();
        m_ui->progressBar->setRange(0,100);
        m_ui->progressBar->setValue(0);
    }
}

bool MainWindow::m_binaryCompare(QString file1, QString file2)
{
    QFile f1(file1);
    QFile f2(file2);
    if( !f1.open(QIODevice::ReadOnly) || !f2.open(QIODevice::ReadOnly))
        return false;

    unsigned int remainingSize = static_cast<unsigned int>(f1.size());
    unsigned int sizeF2 = static_cast<unsigned int>(f2.size());
    // If their sizes are not equal, return false immediately!
    if(remainingSize != sizeF2)
        return false;

    while( !m_cancelExec && (remainingSize > 0) )
    {
        char buffer1[BUFFERSIZE];
        char buffer2[BUFFERSIZE];

        unsigned int readSize = std::min(remainingSize, static_cast<unsigned int>(BUFFERSIZE));
        f1.read(buffer1, readSize);
        f2.read(buffer2, readSize);
        // Compare and if unequal portions are found, return false
        if(0 != std::memcmp(buffer1, buffer2, readSize))
        {
            f1.close();
            f2.close();
            return false;
        }  // endif memcmp
        remainingSize -= readSize;
    }  // endwhile remainingsize

    f1.close();
    f2.close();
    return true;
}

void MainWindow::m_makeFileList()
{
    if(m_cancelExec)
        return;

    m_ui->progressBar->reset();
    QApplication::processEvents();
    m_ui->progressBar->setRange(0, 0);
    QApplication::processEvents();

    m_fList.clear();
    m_fRemList.clear();
    m_hashList.clear();

    m_appendProgressString("Listing Files.");

    QDirIterator::IteratorFlags flags = QDirIterator::NoIteratorFlags;
    if(m_opts.isRecursive)
        flags = QDirIterator::Subdirectories;

    QStringList nameFilters;

    if( !(m_opts.type.isEmpty()) )
    {
        if( !m_opts.type.startsWith("*") )
        {
            if( m_opts.type.startsWith(".") )
                m_opts.type = "*" + m_opts.type;
            else
                m_opts.type = "*." + m_opts.type;
        }
        nameFilters<<m_opts.type;
    }
    else
    {
        nameFilters<<"*";
    }

    QDirIterator it(m_opts.rDir, nameFilters, QDir::Files|QDir::NoDotAndDotDot|QDir::Readable, flags);

    while(!m_cancelExec && it.hasNext())
    {
        QApplication::processEvents();

        QString fName = it.next();
        QMimeDatabase mdb;
        QString mimeType = mdb.mimeTypeForFile(fName).name();

        if( m_opts.isImage && mimeType.contains("image", Qt::CaseInsensitive) &&
                        m_mimeTypeList.contains(mimeType, Qt::CaseInsensitive) )
        {
            m_fList.append(fName);
        }
        else if( m_opts.isVideo && mimeType.contains("video", Qt::CaseInsensitive) &&
                             m_mimeTypeList.contains(mimeType, Qt::CaseInsensitive) )
        {
            m_fList.append(fName);
        }
        else if( m_opts.isAudio && mimeType.contains("audio", Qt::CaseInsensitive) &&
                             m_mimeTypeList.contains(mimeType, Qt::CaseInsensitive) )
        {
            m_fList.append(fName);
        }
        else if( !(m_opts.isImage || m_opts.isVideo || m_opts.isAudio) )
        {
            m_fList.append(fName);
        }

        if(m_opts.isHash)
        {
            QApplication::processEvents();

            QFile inFile(m_fList.last());
            char buffer[BUFFERSIZE];

            if(inFile.open(QIODevice::ReadOnly))
            {
                qint64 remainingSize = inFile.size();
                QCryptographicHash hashSha256(QCryptographicHash::Sha256);

                qint64 readSize = qMin(remainingSize, static_cast<qint64>(BUFFERSIZE));
                qint64 bytesRead = inFile.read(buffer, readSize);
                while( !m_cancelExec && (readSize > 0) && (bytesRead > 0) )
                {
                    hashSha256.addData(buffer, static_cast<int>(bytesRead));
                    remainingSize -= bytesRead;
                    readSize = qMin(remainingSize, static_cast<qint64>(BUFFERSIZE));
                    bytesRead = inFile.read(buffer, readSize);
                }

                QByteArray res = hashSha256.result();
                m_hashList.append(QString().append(res));
                inFile.close();
            }  // endif file.open
        }  // endif ishash
    }  // endwhile hasnext

    m_appendProgressString( QString("File listing done. %1 files listed.").arg(m_fList.size()) );
}

void MainWindow::m_doCompare()
{
    if(m_cancelExec)
        return;

    QApplication::processEvents();

    QString pltfrm = getPlatform();

    // If platform is not supported for multimedia, say so to the user
    if( !(pltfrm=="Windows") && !(pltfrm=="MacOS") && !(pltfrm=="Linux") &&
        (m_opts.isImage||m_opts.isVideo||m_opts.isAudio) )
    {
        m_showWarning(MSG_UNSUPPORTED_OS);
        m_opts.isHash = true;
        m_opts.isImage = false;
        m_opts.isVideo = false;
        m_opts.isAudio = false;
    }  // endif platform

    QApplication::processEvents();
    // Compile the list of files that are relevant
    m_makeFileList();

    QApplication::processEvents();

    // Set the progress dialog, text output etc.
    int len = m_fList.length();
    quint64 tot = static_cast<quint64>((len * (len-1)) / 2);
    m_ui->progressBar->reset();
    m_ui->progressBar->setRange(0,100);
    m_ui->progressBar->setValue(0);
    m_appendProgressString( "Comparison started for :"+ QString("%1").arg(tot) + " combinations." );
    QTextCursor curs;
    m_appendProgressString( QString("%1").arg(0) + "% Complete." );
    // Count of how many comparisons are done
    quint64 cnt = 0;

    for( int i = 0; i < m_fList.length(); ++i )
    {
        if(m_cancelExec)
            return;

        QApplication::processEvents();

        if(m_fRemList.contains(m_fList.at(i)))
        {
            cnt += CASTUINT64(m_fList.length() - i - 1);
            continue;
        }

        QFile f1(m_fList.at(i));

        if( !f1.open(QIODevice::ReadOnly) )
        {
            cnt += CASTUINT64(m_fList.length() - i - 1);
            continue;
        }
        else if( (f1.size() < m_opts.minSize*MEGA) || (f1.size() > m_opts.maxSize*MEGA) || !f1.size() )
        {
            cnt += CASTUINT64(m_fList.length() - i - 1);
            continue;
        }

        // Subset of the files to be compared with the present file
        QStringList tmpList = m_fList;
        tmpList.erase(tmpList.begin(), tmpList.begin()+i+1);
        QStringList tmpHList;
        if(m_opts.isHash)
        {
            tmpHList = m_hashList;
            tmpHList.erase(tmpHList.begin(), tmpHList.begin()+i+1);
        }

        bool isSame = false;

        for( int j = 0; j < tmpList.length(); ++j )
        {
            if(m_cancelExec)
                return;

            QApplication::processEvents();
            cnt++;

            if(m_fRemList.contains(tmpList.at(j)))
                continue;

            QFile f2(tmpList.at(j));
            isSame = false;

            if( !f2.open(QIODevice::ReadOnly) )
                continue;
            else if( (f2.size() < m_opts.minSize*MEGA) || (f2.size() > m_opts.maxSize*MEGA) || !f2.size() )
                continue;

            if(m_opts.isHash)
            {
                if(m_hashList.at(i) == tmpHList.at(j))
                    isSame = true;
            }
            else if(m_opts.isImage)
                isSame = m_mediaCompare->compareImgs( m_fList.at(i), tmpList.at(j) );
            else if(m_opts.isVideo)
                isSame = m_mediaCompare->compareVids( m_fList.at(i), tmpList.at(j) );
            else if(m_opts.isAudio)
                isSame = m_mediaCompare->compareSnds( m_fList.at(i), tmpList.at(j) );
            else
                isSame = m_binaryCompare(m_fList.at(i), tmpList.at(j));

            QFileInfo info1(f1.fileName());
            QFileInfo info2(f2.fileName());

            if(isSame)
            {
                if( (!m_opts.isKeepNew && (info1.lastModified() <= info2.lastModified())) ||
                    (m_opts.isKeepNew && (info1.lastModified() > info2.lastModified())) )
                    m_fRemList.append(tmpList.at(j));
                else if( (!m_opts.isKeepNew && (info1.lastModified() > info2.lastModified())) ||
                         (m_opts.isKeepNew && (info1.lastModified() <= info2.lastModified())) )
                    m_fRemList.append(m_fList.at(i));
            }  // endif issame

            curs = m_ui->progressTxt->textCursor();
            curs.select(QTextCursor::LineUnderCursor);
            curs.insertText(QString("%1").arg(static_cast<int>(100*cnt/tot)) + "% Complete.");
            QApplication::processEvents();
            m_ui->progressTxt->setTextCursor(curs);
            m_ui->progressBar->setValue(static_cast<int>(100*cnt/tot));
            QApplication::processEvents();
        }  // endfor j
    }  // endfor i

    curs = m_ui->progressTxt->textCursor();
    curs.select(QTextCursor::LineUnderCursor);
    curs.insertText(QString("%1").arg(100) + "% Complete.");
    QApplication::processEvents();
    m_ui->progressTxt->setTextCursor(curs);
    m_ui->progressBar->setValue(100);
    QApplication::processEvents();
    m_appendProgressString("End of comparison. Now will do removal of files.");
    m_doRemoval();
    m_ui->progressBar->reset();
    m_ui->progressBar->setRange(0,100);
    m_ui->progressBar->setValue(0);
    QApplication::processEvents();
}

void MainWindow::m_doRemoval()
{
    if(m_cancelExec)
        return;

    m_ui->progressBar->reset();
    QApplication::processEvents();
    m_ui->progressBar->setRange(0, 0);
    QApplication::processEvents();

    if(m_opts.isDryRun) // If dryrun option is selected
    {
        m_appendProgressString("Writing duplicate file list");
        // Get some random string name
        QDateTime time = QDateTime::currentDateTime();
        QString fName = "DuplicateFiles_"+time.toString("dd_MM_yyyy_hh_mm")+"_"+getRandomString(6)+".txt";
        QFile outFile(QDir::cleanPath(m_opts.rDir + "/" + fName));

        if(!outFile.open(QIODevice::WriteOnly | QFile::Text))
        {
            m_showWarning(MSG_DRYRUN_FAILED);
            return;
        }
        // Wrtie the list out to the file
        QTextStream oStr(&outFile);
        for(int i = 0; i < m_fRemList.size(); ++i )
        {
            QApplication::processEvents();

            if(QFile::exists(m_fRemList.at(i)))
                oStr<<m_fRemList.at(i)<<'\n';
        } // endfor _fRemList
        outFile.close();
    }  // endif isdryrun
    else if(m_opts.isAbsDelete) // If absolute delete option is selected
    {
        m_appendProgressString("Removing files");
        for(int i = 0; i <= m_fRemList.size(); ++i)
        {
            QApplication::processEvents();

            QFile f(m_fRemList.at(i));
            if(!f.exists())
                continue;
            // Completely remove the file. If failed, report and do a dryrun instead for the remaining files.
            if( !f.remove() )
            {
                m_showWarning(MSG_DELETING_FAILED);
                m_opts.isDryRun = true;
                m_doRemoval();
                return;
            } // endif remove
        } // endfor _fRemList
    }  // endelseif isabsdelete
    else  // If the files are to be moved to a trash folder
    {
        m_appendProgressString("Moving files to trash folder");
        for(int i = 0; i <= m_fRemList.size(); ++i)
        {
            QFile f(m_fRemList.at(i));
            if(!f.exists())
                continue;
            // Copy and then remove the source file. If failed, report and do a dryrun instead for
            // the remaining files.
            if( !(f.copy(QDir::cleanPath(m_opts.trashDir + "/" + f.fileName()))) || !f.remove() )
            {
                m_showWarning(MSG_DELETING_FAILED);
                m_opts.isDryRun = true;
                m_doRemoval();
                return;
            }  // endif copy and remove
        } // endfor _fRemList
    } // endelse

    m_appendProgressString("Removing Done. All ready to go again!");
    return;
}

void MainWindow::m_showWarning(DisplayMessage msg)
{
    QMessageBox _dialog;
    _dialog.setIcon(QMessageBox::Warning);
    switch(msg)
    {
        case MSG_MAX_FSIZE_4GB:
            _dialog.setText("The maximum file size is set to larger than 4GB. I am not designed to handle such huge files. "
                           "Will perform hash comparison of files up to 4GB now.");
            _dialog.exec();
            break;
        case MSG_MAX_FSIZE_1GB:
            _dialog.setText("The maximum file size is set to larger than 1GB. "
                           "Will perform hash comparison for performance reasons.");
            _dialog.exec();
            break;
        case MSG_UNSUPPORTED_OS:
            _dialog.setText("Image/Video/Audio content comparison not supported on this platform/OS! "
                           "Will perform hash comparison.");
            _dialog.exec();
            break;
        case MSG_ERR_INCORRECT_PATH:
            _dialog.setText("The source path is invalid!");
            _dialog.exec();
            break;
        case MSG_ERR_INCORRECT_TRASH_PATH:
            _dialog.setText("The trash path is invalid!");
            _dialog.exec();
            break;
        case MSG_DRYRUN_FAILED:
            _dialog.setText("DryRun failed. Please check whether you have write permission to the path and try again.");
            _dialog.exec();
            break;
        case MSG_DELETING_FAILED:
            _dialog.setText("Delete failed for some file(s). Will abort physical deletion now and "
                            "perform DryRun on the rest of the files, i.e., write list of duplicate files to a txt file.");
            _dialog.exec();
            break;
        case MSG_MAX_LESSTHAN_MIN:
            _dialog.setText("Max file size is less than the Min file size!");
            _dialog.exec();
            break;
        case MSG_MAX_OR_MIN_LESSTHAN_ZERO:
            _dialog.setText("Max or Min file size is less than zero.");
            _dialog.exec();
            break;
        default:
            break;
    }
}

bool MainWindow::m_showSureDlg()
{
    QMessageBox sureDlg;
    QAbstractButton * btnY = sureDlg.addButton(QMessageBox::Yes);
    QAbstractButton * btnN = sureDlg.addButton(QMessageBox::No);
    Q_UNUSED(btnN);
    sureDlg.setDefaultButton(QMessageBox::No);
    sureDlg.setText("Are you sure you want to cancel the execution?");

    sureDlg.exec();

    if(sureDlg.clickedButton() == btnY)
        return true;
    else
        return false;
}

void MainWindow::m_appendProgressString(QString in)
{
     m_ui->progressTxt->append( "<span style=\"color:rgb(200,200,200)\">"+in+"</span>" );
}

bool MainWindow::m_parseOptions()
{
    m_opts.rDir     = m_ui->pathEdit->text();
    m_opts.type     = m_ui->typeEdit->text();
    m_opts.trashDir = m_ui->trashEdit->text();

    QDir dir(m_opts.rDir);
    if( m_opts.rDir.isEmpty() || m_opts.rDir.isNull() || !dir.exists() )
    {
        m_showWarning(MSG_ERR_INCORRECT_PATH);
        return false;
    }

    m_opts.isDryRun    = m_ui->dryrunBox->isChecked();
    m_opts.isRecursive = m_ui->recurseBox->isChecked();
    m_opts.isKeepNew   = m_ui->keepnewBox->isChecked();
    m_opts.isImage     = m_ui->imageBox->isChecked();
    m_opts.isVideo     = m_ui->videoBox->isChecked();
    m_opts.isAudio     = m_ui->audioBox->isChecked();
    m_opts.isAbsDelete = m_ui->absBox->isChecked();
    m_opts.isHash      = m_ui->hashBox->isChecked();

    if( !(m_opts.isAbsDelete || m_opts.isDryRun) )
    {
        QDir dir(m_opts.trashDir);
        if( m_opts.trashDir.isEmpty() || m_opts.trashDir.isNull() || !dir.exists() )
        {
            m_showWarning(MSG_ERR_INCORRECT_TRASH_PATH);
            return false;
        }
    }

    bool okmin, okmax;
    float tmp1 = m_ui->minSizeEdit->text().toFloat(&okmin);
    float tmp2 = m_ui->maxSizeEdit->text().toFloat(&okmax);
    if(okmin)
    {
        if(tmp1 < 0)
        {
            m_showWarning(MSG_MAX_OR_MIN_LESSTHAN_ZERO);
            return false;
        }
        m_opts.minSize = tmp1;
    }
    if(okmax)
    {
        if(tmp2 < tmp1)
        {
            m_showWarning(MSG_MAX_LESSTHAN_MIN);
            return false;
        }
        if(tmp2 <= 0)
        {
            m_showWarning(MSG_MAX_OR_MIN_LESSTHAN_ZERO);
            return false;
        }

        if( tmp2 >= 1024 )
        {
            if( tmp2 >= 4096 )
            {
                m_showWarning(MSG_MAX_FSIZE_4GB);
                tmp2 = 4096;
            }
            else
            {
                m_showWarning(MSG_MAX_FSIZE_1GB);
            }
            m_opts.isHash = true;
        }
        m_opts.maxSize = tmp2;
    }

    if(m_opts.isHash)
    {
        m_opts.isImage = false;
        m_opts.isVideo = false;
        m_opts.isAudio = false;
    }

    if(m_opts.isDryRun)
        m_opts.isAbsDelete = false;

    return true;
}

void MainWindow::m_setFields(Options opts)
{
    m_ui->pathEdit->clear();
    m_ui->typeEdit->clear();
    m_ui->trashEdit->clear();
    m_ui->minSizeEdit->clear();
    m_ui->maxSizeEdit->clear();

    m_ui->pathEdit->setText( opts.rDir );
    m_ui->typeEdit->setText( opts.type );
    m_ui->trashEdit->setText( opts.trashDir );
    m_ui->minSizeEdit->setText( QString("%1").arg(CASTDOUBLE(opts.minSize)) );
    m_ui->maxSizeEdit->setText( QString("%1").arg(CASTDOUBLE(opts.maxSize)) );

    m_ui->dryrunBox->setChecked( opts.isDryRun );
    m_ui->recurseBox->setChecked( opts.isRecursive );
    m_ui->keepnewBox->setChecked( opts.isKeepNew );
    m_ui->imageBox->setChecked( opts.isImage );
    m_ui->videoBox->setChecked( opts.isVideo );
    m_ui->audioBox->setChecked( opts.isAudio );
    m_ui->absBox->setChecked( opts.isAbsDelete );
    m_ui->hashBox->setChecked( opts.isHash );
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if( m_cancelExec )
    {
        event->accept();
    }
    else if( m_showSureDlg() )
    {
        m_cancelExec = true;
        event->accept();
    }
    else
    {
        event->ignore();
    }
}

void MainWindow::on_advButton_clicked()
{
    m_advOpts->show();
}

void MainWindow::handleAdvancedOptionsSet()
{
    m_advOpts->getOptions(m_imgParams);
    m_advOpts->getOptions(m_videoParams);
    m_advOpts->getOptions(m_audioParams);
}
