#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>
#include <QMainWindow>
#include <QList>
#include "form.h"
#include "advancedopts.h"
#include "internaldefs.h"
#include "mediacompare.h"

enum DisplayMessage
{
    MSG_MAX_FSIZE_4GB,      // Max file size is set above 8GB, we will only do binary comparison of files upto 4GB.
    MSG_MAX_FSIZE_1GB,      // Max file size is set above 1GB, we will only do binary comparison.
    MSG_UNSUPPORTED_OS,     // Will perform only binary comparison since media comparison is not supported in this OS.
    MSG_ERR_INCORRECT_PATH, // The root directory is empty or invalid.
    MSG_ERR_INCORRECT_TRASH_PATH, // The trash directory is invalid.
    MSG_DRYRUN_FAILED,      // DryRun failed, no write permission?
    MSG_DELETING_FAILED,    // Deleting failed, will try a dry run.
    MSG_MAX_LESSTHAN_MIN,   // Max file size is less than the min file size.
    MSG_MAX_OR_MIN_LESSTHAN_ZERO,  // Max of min file size is less than zero.
    MSG_INVALID             // Invalid msg from here on.
};

struct Options
{
    bool isDryRun    = true;  // When true, the duplicates ar eonly listed out in a text file and not actually deleted.
    bool isRecursive = true;  // When true, performs recursive search i nthe directory.
    bool isKeepNew   = false; // When true, keeps the newer file and deletes the older.
    bool isImage     = false; // When true, looks only for image files and compares only them.
    bool isVideo     = false; // When true, looks only for video files and compares only them.
    bool isAudio     = false; // When true, looks only for audio files and compares only them.
    bool isAbsDelete = false; // When true, perfroms only absolute binary comparisons.
    bool isHash      = true;  // When true, compare sonly hash values. Most of the time, good for text and binary files.

    QString rDir     = ""; // The root directory.
    QString type     = ""; // File extension filter.
    QString trashDir = ""; // Trash directory to be used to move the files if not to be deleted.

    float minSize = 0.0f;    // Minimum file size to be considered.
    float maxSize = 1000.0f; // Maximum file size to be considered.
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() override;

private slots:
    void on_helpButton_clicked();

    void on_quitButton_clicked();

    void on_resetButton_clicked();

    void on_mainButton_clicked();

    void on_pathButton_clicked();

    void on_trashButton_clicked();

    void on_cancelBtn_clicked();

    void on_advButton_clicked();

    void handleAdvancedOptionsSet();

private:
    /*
     * Compares the two files binarily
     */
    bool m_binaryCompare(QString file1, QString file2);

    /*
     * Makes a list of files based o nthe internal variables.
     */
    void m_makeFileList();

    /*
     * Main function to compare files etc.
     */
    void m_doCompare();

    /*
     * Function to actually perfrom the removal.
     */
    void m_doRemoval();

    void m_showWarning(DisplayMessage msg);

    bool m_showSureDlg();

    inline void m_appendProgressString(QString in);

    bool m_parseOptions();

    void m_setFields(Options opts);

    virtual void closeEvent(QCloseEvent * e) override;


    Ui::MainWindow * m_ui;

    Form * m_myHelpForm;

    AdvancedOpts * m_advOpts;

    bool m_cancelExec;     // When true, the loops for comparison exits.

    Options m_opts;

    QStringList m_fList;    // List of files
    QStringList m_fRemList; // List of files to remove
    QStringList m_hashList; // List of hash values

    QStringList m_mimeTypeList;

    std::unique_ptr<MediaCompare> m_mediaCompare;

    VideoParams m_videoParams;
    AudioParams m_audioParams;
    ImgParams   m_imgParams;
};

#endif // MAINWINDOW_H
