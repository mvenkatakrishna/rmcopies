#include <cstring>
#include <QDirIterator>
#include <QMimeDatabase>
#include <QCryptographicHash>
#include <QFile>
#include <QFileInfo>
#include <QtMath>
#include <QDateTime>
#include <QTime>
#include <QTextCursor>
#include <QMessageBox>
#include <QThread>

#include "worker.h"
#include "ui_worker.h"

#include <QDebug>

#define BUFFERSIZE 4096

QString getPlatform()
{
#ifdef Q_OS_WIN
    return "Windows";
#endif
#ifdef Q_OS_MAC
    return "MacOS";
#endif
#ifdef Q_OS_LINUX
    return "Linux";
#endif
    return "Unknown";
}

QString getRandomString(quint8 randomStringLength)
{
   const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

   QString randomString;
   for(int i=0; i<randomStringLength; ++i)
   {
       int index = qrand() % possibleCharacters.length();
       QChar nextChar = possibleCharacters.at(index);
       randomString.append(nextChar);
   }
   return randomString;
}

Worker::Worker(QWidget *parent):
    QWidget(parent),
    ui(new Ui::Worker)
{
    ui->setupUi(this);

    _isDryRun    = true;
    _isRecursive = false;
    _isKeepNew   = false;
    _isImage     = false;
    _isVideo     = false;
    _isAudio     = false;
    _isAbsDelete = false;
    _isHash      = true;

    _cancelExec = false;

    _rDir = "";
    _type = "";
    _trashDir = "";

    _minSize = 0;
    _maxSize = 1000;

    _fList.clear();
    _fRemList.clear();
    _hashList.clear();

    // :TODO: Complete the lists
    // Image types supported
    _mimeTypeList<<"image/bmp"<<"image/jp2"<<"image/jpeg"<<"image/png"<<"image/tiff"<<"image/x-tga"<<
                   "image/x-win-bitmap"<<"image/x-xbitmap";
    // Video types supported
    _mimeTypeList<<"video/3gpp"<<"video/3gpp2"<<"video/h261"<<"video/h263"<<"video/h264"<<"video/jpeg"<<
                   "video/jpm"<<"video/mp4"<<"video/mpeg"<<"video/ogg"<<"video/quicktime"<<"video/webm"<<
                   "video/x-f4v"<<"video/x-fli"<<"video/x-flv"<<"video/x-m4v"<<"video/x-matroska"<<
                   "video/x-ms-asf"<<"video/x-ms-wm"<<"video/x-ms-wmv"<<"video/x-ms-wmx";
    // Audio types supported
    _mimeTypeList<<"audio/mpeg"<<"audio/x-mpeg"<<"audio/mpeg3"<<"audio/x-mpeg-3"<<"audio/wav"<<
                   "audio/x-wav"<<"audio/ogg"<<"audio/midi"<<"audio/x-midi"<<"audio/x-raw"<<
                   "audio/mp4"<<"audio/aiff"<<"audio/x-aiff";

    _mediaCompare = std::make_unique<MediaCompare>();
}

Worker::~Worker()
{
    delete ui;
}

bool Worker::_binaryCompare(QString file1, QString file2)
{
    QFile f1(file1);
    QFile f2(file2);
    if( !f1.open(QIODevice::ReadOnly) || !f2.open(QIODevice::ReadOnly))
    {
        return false;
    }

    unsigned int remainingSize = static_cast<unsigned int>(f1.size());
    unsigned int sizeF2 = static_cast<unsigned int>(f2.size());
    // If any of the files is of zero size or their sizes are not equal, return false immediately!
    if( !remainingSize || !sizeF2 || !(remainingSize == sizeF2) )
    {
        return false;
    }

    while (remainingSize > 0)
    {
        char buffer1[BUFFERSIZE];
        char buffer2[BUFFERSIZE];

        unsigned int readSize = qMin(remainingSize, static_cast<unsigned int>(BUFFERSIZE));
        f1.read(buffer1, readSize);
        f2.read(buffer2, readSize);
        // Compare and if unequal portions are found, return false
        if(0 != std::memcmp(buffer1, buffer2, readSize))
        {
            f1.close();
            f2.close();
            return false;
        }  // endif memcmp
        remainingSize -= readSize;
    }  // endwhile remainingsize

    f1.close();
    f2.close();
    return true;
}

void Worker::_makeFileList()
{
    if(_cancelExec)
    {
        return;
    }

    ui->progressBar->reset();
    QApplication::processEvents();
    ui->progressBar->setRange(0, 0);
    QApplication::processEvents();

    _fList.clear();
    _fRemList.clear();
    _hashList.clear();

    ui->progressTxt->appendPlainText("Listing Files.");

    QDirIterator::IteratorFlags flags = QDirIterator::NoIteratorFlags;
    if(_isRecursive)
    {
        flags = QDirIterator::Subdirectories;
    }

    QStringList nameFilters;

    if( !(_type.isEmpty()) )
    {
        if( !_type.startsWith("*") )
        {
            if( _type.startsWith(".") )
                _type = "*" + _type;
            else {
                _type = "*." + _type;
            }
        }
        nameFilters<<_type;
    }
    else
    {
        nameFilters<<"*";
    }

    QDirIterator it(_rDir, nameFilters, QDir::Files|QDir::NoDotAndDotDot|QDir::Readable, flags);

    while(it.hasNext())
    {
        if(_cancelExec)
            return;

        QApplication::processEvents();

        QString fName = it.next();
        QMimeDatabase mdb;
        QString mimeType = mdb.mimeTypeForFile(fName).name();

        if( _isImage && mimeType.contains("image", Qt::CaseInsensitive) &&
                        _mimeTypeList.contains(mimeType, Qt::CaseInsensitive) )
        {
            _fList.append(fName);
        }
        else if( _isVideo && mimeType.contains("video", Qt::CaseInsensitive) &&
                             _mimeTypeList.contains(mimeType, Qt::CaseInsensitive) )
        {
            _fList.append(fName);
        }
        else if( _isAudio && mimeType.contains("audio", Qt::CaseInsensitive) &&
                             _mimeTypeList.contains(mimeType, Qt::CaseInsensitive) )
        {
            _fList.append(fName);
        }
        else if( !(_isImage || _isVideo || _isAudio) )
        {
            _fList.append(fName);
        }

        if(_isHash)
        {
            QApplication::processEvents();

            QFile inFile(_fList.last());
            char buffer[BUFFERSIZE];

            if(inFile.open(QIODevice::ReadOnly))
            {
                qint64 remainingSize = inFile.size();
                QCryptographicHash hashSha256(QCryptographicHash::Sha256);

                qint64 readSize = qMin(remainingSize, static_cast<qint64>(BUFFERSIZE));
                qint64 bytesRead = inFile.read(buffer, readSize);
                while( (readSize > 0) && (bytesRead > 0) )
                {
                    if(_cancelExec)
                    {
                        inFile.close();
                        return;
                    }
                    hashSha256.addData(buffer, static_cast<int>(bytesRead));
                    remainingSize -= bytesRead;
                    readSize = qMin(remainingSize, static_cast<qint64>(BUFFERSIZE));
                    bytesRead = inFile.read(buffer, readSize);
                }

                QByteArray res = hashSha256.result();
                _hashList.append(QString().append(res));
                inFile.close();
            }  // endif file.open
        }  // endif ishash
    }  // endwhile hasnext

    ui->progressTxt->appendPlainText(QString("File listing done. %1 files listed.").arg(_fList.size()));
}

void Worker::_doRemoval()
{
    if(_cancelExec)
        return;

    ui->progressTxt->appendPlainText("Removing files.");
    ui->progressBar->reset();
    QApplication::processEvents();
    ui->progressBar->setRange(0, 0);
    QApplication::processEvents();

    if(_isDryRun) // If dryrun option is selected
    {
        // Get some random string name
        QDateTime time = QDateTime::currentDateTime();
        QString fName = "DuplicateFiles_"+time.toString("dd_MM_yyyy_hh_mm")+"_"+getRandomString(6)+".txt";
        QFile outFile(QDir::cleanPath(_rDir + "/" + fName));

        if(!outFile.open(QIODevice::WriteOnly | QFile::Text))
        {
            _showWarning(MSG_DRYRUN_FAILED);
            return;
        }
        // Wrtie the list out to the file
        QTextStream oStr(&outFile);
        for(int i = 0; i < _fRemList.size(); ++i )
        {
            QApplication::processEvents();

            if(QFile::exists(_fRemList.at(i)))
                oStr<<_fRemList.at(i)<<'\n';
        } // endfor _fRemList
        outFile.close();
    }  // endif isdryrun
    else if(_isAbsDelete) // If absolute delete option is selected
    {
        for(int i = 0; i <= _fRemList.size(); ++i)
        {
            QApplication::processEvents();

            QFile f(_fRemList.at(i));
            if(!f.exists())
                continue;
            // Completely remove the file. If failed, report and do a dryrun instead for the remaining files.
            if( !f.remove() )
            {
                _showWarning(MSG_DELETING_FAILED);
                _isDryRun = true;
                _doRemoval();
                return;
            } // endif remove
        } // endfor _fRemList
    }  // endelseif isabsdelete
    else  // If the files are to be moved to a trash folder
    {
        for(int i = 0; i <= _fRemList.size(); ++i)
        {
            QFile f(_fRemList.at(i));
            if(!f.exists())
                continue;
            // Copy and then remove the source file. If failed, report and do a dryrun instead for
            // the remaining files.
            if( !(f.copy(QDir::cleanPath(_trashDir + "/" + f.fileName()))) || !f.remove() )
            {
                _showWarning(MSG_DELETING_FAILED);
                _isDryRun = true;
                _doRemoval();
                return;
            }  // endif copy and remove
        } // endfor _fRemList
    } // endelse

    ui->progressTxt->appendPlainText("Removing Done. Will close now...");
    return;
}

void Worker::_showWarning(WorkerMessage msg)
{
    QMessageBox _dialog;
    _dialog.setIcon(QMessageBox::Warning);
    switch(msg)
    {
        case MSG_MAX_FSIZE_4GB:
            _dialog.setText("The maximum file size is set to larger than 4GB. I am not designed to handle such huge files. "
                           "Will perform hash comparison of files up to 4GB now.");
            _dialog.exec();
            break;
        case MSG_MAX_FSIZE_1GB:
            _dialog.setText("The maximum file size is set to larger than 1GB. "
                           "Will perform hash comparison for performance reasons.");
            _dialog.exec();
            break;
        case MSG_UNSUPPORTED_OS:
            _dialog.setText("Image/Video/Audio content comparison not supported on this platform/OS! "
                           "Will perform hash comparison.");
            _dialog.exec();
            break;
        case MSG_ERR_INCORRECT_PATH:
            _dialog.setText("The source/trash path is invalid!");
            _dialog.exec();
            break;
        case MSG_DRYRUN_FAILED:
            _dialog.setText("DryRun failed. Please check whether you have write permission to the path and try again.");
            _dialog.exec();
            break;
        case MSG_DELETING_FAILED:
            _dialog.setText("Delete failed for some file(s). Will abort physical deletion now and "
                            "perform DryRun on the rest of the files, i.e., write list of duplicate files to a txt file.");
            _dialog.exec();
            break;
        default:
            break;
    }
}

bool Worker::_showSureDlg()
{
    QMessageBox sureDlg;
    QAbstractButton * btnY = sureDlg.addButton(QMessageBox::Yes);
    QAbstractButton * btnN = sureDlg.addButton(QMessageBox::No);
    Q_UNUSED(btnN);
    sureDlg.setDefaultButton(QMessageBox::No);
    sureDlg.setText("Are you sure you want to cancel the execution?");

    sureDlg.exec();

    if(sureDlg.clickedButton() == btnY)
        return true;
    else
        return false;
}

void Worker::setParams(bool *boList, QStringList* strList, qint64 *intList)
{
    _cancelExec = false;

    _isDryRun    = boList[0];
    _isRecursive = boList[1];
    _isKeepNew   = boList[2];
    _isImage     = boList[3];
    _isVideo     = boList[4];
    _isAudio     = boList[5];
    _isAbsDelete = boList[6];
    _isHash      = boList[7];

    _rDir     = strList->at(0);
    _type     = strList->at(1);
    _trashDir = strList->at(2);

    QDir dir(_rDir);
    if( _rDir.isEmpty() || _rDir.isNull() || !dir.exists() )
    {
        _showWarning(MSG_ERR_INCORRECT_PATH);
        _cancelExec = true;
    }

    _minSize = intList[0];
    _maxSize = intList[1];

    if( _maxSize >= 1024 )
    {
        if( _maxSize >= 4096 )
        {
            _showWarning(MSG_MAX_FSIZE_4GB);
            _maxSize = 4096;
        }
        else
        {
            _showWarning(MSG_MAX_FSIZE_1GB);
        }
        _isHash = true;
    }

    if( _isHash )
    {
        _isImage = false;
        _isVideo = false;
        _isAudio = false;
    }

    if(_isImage)
        _mediaCompare->setImgParams(_imgParams);
    else if(_isVideo)
        _mediaCompare->setVideoParams(_videoParams);
    else if(_isAudio)
        _mediaCompare->setAudioParams(_audioParams);
}

void Worker::doCompare()
{
    if(_cancelExec)
    {
        this->close();
        return;
    }

    this->show();
    QApplication::processEvents();

    QString pltfrm = getPlatform();

    // If platform is not supported for multimedia, say so to the user
    if( !(pltfrm=="Windows") && !(pltfrm=="MacOS") && !(pltfrm=="Linux") && (_isImage||_isVideo||_isAudio) )
    {
        _showWarning(MSG_UNSUPPORTED_OS);
        _isHash = true;
        _isImage = false;
        _isVideo = false;
        _isAudio = false;
    }  // endif platform

    QApplication::processEvents();
    // Compile the list of files that are relevant
    _makeFileList();

    QApplication::processEvents();

    // Set the progress dialog, text output etc.
    int len = _fList.length();
    quint64 tot = static_cast<quint64>((len * (len-1)) / 2);
    ui->progressBar->reset();
    ui->progressBar->setRange(0,100);
    ui->progressBar->setValue(0);
    ui->progressTxt->appendPlainText("Comparison started for :"+ QString("%1").arg(tot) + " combinations.");
    QTextCursor curs;
    ui->progressTxt->appendPlainText(QString("%1").arg(0) + "% Complete.");
    // Count of how many comparisons are done
    quint64 cnt = 0;

    for( int i = 0; i < _fList.length(); ++i )
    {
        if(_cancelExec)
        {
            this->close();
            return;
        }

        QApplication::processEvents();

        if(_fRemList.contains(_fList.at(i)))
        {
            cnt += static_cast<quint64>(_fList.length() - i - 1);
            continue;
        }

        QFile f1(_fList.at(i));

        qint64 million = static_cast<qint64>(qPow(10,6));

        if( !f1.open(QIODevice::ReadOnly) )
        {
            cnt += static_cast<quint64>(_fList.length() - i - 1);
            continue;
        }
        else if( (f1.size() < _minSize*million) || (f1.size() > _maxSize*million) || !f1.size() )
        {
            cnt += static_cast<quint64>(_fList.length() - i - 1);
            continue;
        }

        // Subset of the files to be compared with the present file
        QStringList tmpList = _fList;
        tmpList.erase(tmpList.begin(), tmpList.begin()+i+1);
        QStringList tmpHList;
        if(_isHash)
        {
            tmpHList = _hashList;
            tmpHList.erase(tmpHList.begin(), tmpHList.begin()+i+1);
        }

        bool isSame = false;

        for( int j = 0; j < tmpList.length(); ++j )
        {
            if(_cancelExec)
            {
                this->close();
                return;
            }

            QApplication::processEvents();
            cnt++;

            if(_fRemList.contains(tmpList.at(j)))
                continue;

            QFile f2(tmpList.at(j));
            isSame = false;

            if( !f2.open(QIODevice::ReadOnly) )
                continue;
            else if( (f2.size() < _minSize*million) || (f2.size() > _maxSize*million) || !f2.size() )
                continue;

            if(_isHash)
            {
                if(_hashList.at(i) == tmpHList.at(j))
                    isSame = true;
            }
            else if(_isImage)
                isSame = _mediaCompare->compareImgs( _fList.at(i), tmpList.at(j) );
            else if(_isVideo)
                isSame = _mediaCompare->compareVids( _fList.at(i), tmpList.at(j) );
            else if(_isAudio)
                isSame = _mediaCompare->compareSnds( _fList.at(i), tmpList.at(j) );
            else
                _binaryCompare(_fList.at(i), tmpList.at(j));

            QFileInfo info1(f1.fileName());
            QFileInfo info2(f2.fileName());

            if(isSame)
            {
                if( (!_isKeepNew && (info1.lastModified() <= info2.lastModified())) || (_isKeepNew && (info1.lastModified() > info2.lastModified())) )
                    _fRemList.append(tmpList.at(j));
                else if( (!_isKeepNew && (info1.lastModified() > info2.lastModified())) || (_isKeepNew && (info1.lastModified() <= info2.lastModified())) )
                    _fRemList.append(_fList.at(i));
            }  // endif issame

            curs = ui->progressTxt->textCursor();
            curs.select(QTextCursor::LineUnderCursor);
            curs.insertText(QString("%1").arg(static_cast<int>(100*cnt/tot)) + "% Complete.");
            QApplication::processEvents();
            ui->progressTxt->setTextCursor(curs);
            ui->progressBar->setValue(static_cast<int>(100*cnt/tot));
            QApplication::processEvents();
        }  // endfor j
    }  // endfor i

    curs = ui->progressTxt->textCursor();
    curs.select(QTextCursor::LineUnderCursor);
    curs.insertText(QString("%1").arg(100) + "% Complete.");
    QApplication::processEvents();
    ui->progressTxt->setTextCursor(curs);
    ui->progressBar->setValue(100);
    QApplication::processEvents();
    ui->progressTxt->appendPlainText("End of comparison. Now will do removal of files.");
    _doRemoval();
    QApplication::processEvents();
    QThread::sleep(2);
    _cancelExec = true;
    this->close();
}

void Worker::on_cancelBtn_clicked()
{
    if( _showSureDlg() )
    {
        _cancelExec = true;
        this->close();
    }
}

void Worker::closeEvent(QCloseEvent *event)
{
    if( _cancelExec )
    {
        event->accept();
    }
    else if( _showSureDlg() )
    {
        _cancelExec = true;
        event->accept();
    }
    else
    {
        event->ignore();
    }
}
