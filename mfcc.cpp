#include <complex>
#include <algorithm>
#include <numeric>
#include <cmath>

#include "mfcc.h"

namespace
{

inline float HZ2MEL(float x)
{
    return 2595.0f * std::log10( 1.0f + (x/700.0f) );
}

inline float MEL2HZ(float x)
{
    return 700.0f*( std::pow(10.0f, (x/2595.0f)) - 1.0f );
}

}

// :TODO: Try to bring in more eigen matrix functions to replace nested for loops.

MFCC::MFCC(AudioParams params):m_params(params)
{
    m_generateHammingWdw();
    m_generateDctMat();
    m_generateTrifBank();
}

MFCC::~MFCC()
{ }

Eigen::MatrixXf MFCC::getMfcc(const QVector<int16_t> samples)
{
    uint32_t numFrames = (CASTUINT(samples.size()) - m_params.FRAME_LEN + m_params.FRAME_SHIFT)/m_params.FRAME_SHIFT;
    m_mfcc = Eigen::MatrixXf::Zero(numFrames, m_params.NUM_CEPSTRA);

    uint32_t offset = 0;
    for(uint32_t i = 0; i < numFrames; ++i)
    {
        if( offset + m_params.FRAME_LEN > CASTUINT(samples.length()) )
            break;
        std::vector<float> frame( samples.begin()+offset, samples.begin()+offset+m_params.FRAME_LEN );
        m_processFrame( frame, CASTINT(i) );
        offset += m_params.FRAME_SHIFT;
    }

    return m_mfcc;
}

void MFCC::m_generateHammingWdw()
{
    m_hammingWdw.resize(m_params.FRAME_LEN);
    for(uint32_t i = 0; i < m_params.FRAME_LEN; ++i)
    {
        m_hammingWdw[i] = 0.54f - (0.46f * std::cos( 2 * 3.1415926f * i / m_params.FRAME_LEN ));
    }
}

void MFCC::m_generateDctMat()
{
    m_dctMat.resize( (m_params.NUM_CEPSTRA+1), m_params.NUM_FILTERS );

    for(int i = 0; i < m_params.NUM_CEPSTRA+1; ++i)
    {
        float c = (i==0)?std::sqrt(1.0f/m_params.NUM_FILTERS):std::sqrt(2.0f/m_params.NUM_FILTERS);
        for(int j = 0; j < m_params.NUM_FILTERS; ++j)
        {
            m_dctMat(i,j) = c * std::cos(3.1415926f * i * (j+0.5f) / m_params.NUM_FILTERS);
        }
    }
}

void MFCC::m_generateTrifBank()
{
    m_trifBank.resize( m_params.NUM_FILTERS, m_params.NUM_FFT_BINS );
    float low_Mel = HZ2MEL(m_params.MIN_FREQ);
    float hig_Mel = HZ2MEL(m_params.MAX_FREQ);

    std::vector<float> cutoffF(m_params.NUM_FILTERS+2);
    for(uint32_t i = 0; i < CASTUINT(m_params.NUM_FILTERS+2); ++i)
    {
        float tmp = low_Mel + ((hig_Mel - low_Mel)*i)/(m_params.NUM_FILTERS+1);
        cutoffF[i] = MEL2HZ(tmp);
    }

    std::vector<float> fftF(m_params.NUM_FFT_BINS);
    for(uint32_t i = 0; i < m_params.NUM_FFT_BINS; ++i)
    {
        // Equivalent to matlab linspace(low, hig, NUM_FFT_BINS)
        fftF[i] = (m_params.SAMPLE_RATE/2.0f)*(i/(m_params.NUM_FFT_BINS-1));
    }

    for(uint32_t i = 0; i < m_params.NUM_FILTERS; ++i )
    {
        for(uint32_t j = 0; j < m_params.NUM_FFT_BINS; ++j)
        {
            float coeff = 0.0f;
            if(fftF[j] <= cutoffF[i+1])
                coeff = 0.0f;
            else if(fftF[j] <= cutoffF[i+1])
                coeff = (fftF[j] - cutoffF[i])/(cutoffF[i+1] - cutoffF[i]);
            else if(fftF[j] <= cutoffF[i+2] )
                coeff = (cutoffF[i+2] - fftF[j])/(cutoffF[i+2] - cutoffF[i+1]);
            m_trifBank(i,j) = coeff;
        }
    }
}

void MFCC::m_processFrame(std::vector<float>& frame, int frameIdx)
{
    // Apply pre emphasis and hamming window
    float prevSample = frame[0];
    float currSample;
    frame[0] *= m_hammingWdw[0];
    for(unsigned int i = 1; i < m_params.FRAME_LEN; ++i)
    {
        currSample = frame[i];
        frame[i] = m_hammingWdw[i] * ( currSample - (m_params.PRE_EMPH * prevSample) );
        prevSample = currSample;
    }

    // Get FFT
    std::vector<std::complex<float>> fftVec;
    Eigen::FFT<float> fft;
    fft.fwd(fftVec, frame);
    // Extract the magnitude spectrum
    std::vector<float> magSpec(m_params.NUM_FFT_BINS);
    std::transform( fftVec.begin(), (fftVec.begin()+m_params.NUM_FFT_BINS), magSpec.begin(),
                    [](std::complex<float> a){return ( (a.real()*a.real()) + (a.imag()*a.imag()) );} );

    // Apply the triangular filter bank and get the log output
    std::vector<float> filtOut(m_params.NUM_FILTERS, 0.0f);
    Eigen::VectorXf filterOut = Eigen::VectorXf::Zero(m_params.NUM_FILTERS);
    Eigen::Map<Eigen::VectorXf> vec_magSpec(magSpec.data(), CASTUINT(magSpec.size()));
    for(unsigned int i = 0; i < m_params.NUM_FILTERS; ++i)
    {
        filterOut(i) = std::log( std::max(1.0f,m_trifBank.row(i).dot(vec_magSpec)) );
    }

    // Finally calculate the MFCC as the DCT of the filter bank output
    for(unsigned int i = 0; i < m_params.NUM_CEPSTRA; ++i)
    {
        m_mfcc(frameIdx,CASTINT(i)) = m_dctMat.row(i).dot(filterOut);
    }
}
