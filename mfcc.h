#ifndef MFCC_H
#define MFCC_H

#include <vector>
#include <QVector>
#include <eigen/Core>
#include <eigen/FFT>

#include "internaldefs.h"

class MFCC
{
public:
    MFCC(AudioParams params);
    ~MFCC();

    Eigen::MatrixXf getMfcc(const QVector<int16_t> samples);

private:
    void m_generateHammingWdw();
    void m_generateDctMat();
    void m_generateTrifBank();

    void m_processFrame(std::vector<float>& frame, int frameIdx);

    Eigen::MatrixXf m_mfcc;
    AudioParams m_params;

    std::vector<float> m_hammingWdw;
    Eigen::MatrixXf m_dctMat;
    Eigen::MatrixXf m_trifBank;
};

#endif // MFCC_H
