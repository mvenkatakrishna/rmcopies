#include "advancedopts.h"
#include "ui_advancedopts.h"

#include <QMessageBox>

AdvancedOpts::AdvancedOpts(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdvancedOpts)
{
    ui->setupUi(this);
    this->setWindowModality(Qt::ApplicationModal);

    QStringList l;
    l<<"64"<<"128"<<"256"<<"512"<<"1024"<<"2048"<<"4096";
    ui->img_sizeBox->addItems(l);
    ui->vid_sizeBox->addItems(l);
    l.clear();
    l<<"8"<<"16"<<"32"<<"64"<<"128"<<"256"<<"512";
    ui->img_blockszBox->addItems(l);

    on_resetBtn_clicked();
}

AdvancedOpts::~AdvancedOpts()
{
    delete ui;
}

void AdvancedOpts::getOptions(ImgParams &imgOpts)
{
    imgOpts = m_imgOpts;
}

void AdvancedOpts::getOptions(VideoParams &videoOpts)
{
    videoOpts = m_videoOpts;
}

void AdvancedOpts::getOptions(AudioParams &audioOpts)
{
    audioOpts = m_audioOpts;
}

void AdvancedOpts::on_cancelBtn_clicked()
{
    this->close();
}

void AdvancedOpts::on_doneBtn_clicked()
{
    // Read Image options
    m_imgOpts.IMG_SIZE = ui->img_sizeBox->currentText().toInt();
    m_imgOpts.IMG_BLOCK_SIZE = ui->img_blockszBox->currentText().toInt();
    m_imgOpts.IMG_THRESHOLD = ui->img_thrLabel->text().toFloat();
    m_imgOpts.IMG_MED_THRESHOLD = ui->img_mthrLabel->text().toFloat();
    if(m_imgOpts.IMG_BLOCK_SIZE > (m_imgOpts.IMG_SIZE/4))
    {
        QMessageBox::warning(this,"Warning", "Block size must be smaller than (image size / 4)");
        return;
    }

    // Read Audio options
    m_audioOpts.MAX_DURATION_DIFF = CASTREAL(ui->aud_diffSlider->value())/100.0f;
    m_audioOpts.THRESHOLD = ui->aud_thrLabel->text().toFloat();

    // Read Video options
    m_videoOpts.IMG_SIZE = ui->vid_sizeBox->currentText().toInt();
    m_videoOpts.THRESHOLD = ui->vid_thrLabel->text().toFloat();
    m_videoOpts.MAX_DURATION_DIFF = CASTREAL(ui->vid_diffSlider->value())/100.0f;

    // Emit the signal
    emit advancedOptionsSet();
}

void AdvancedOpts::on_resetBtn_clicked()
{
    on_img_resetBtn_clicked();
    on_vid_resetBtn_clicked();
    on_aud_resetBtn_clicked();
}

void AdvancedOpts::on_img_thrSlider_sliderMoved(int position)
{
    QString s;
    s = s.setNum(position/10.0f,'f',1);
    ui->img_thrLabel->setText( s );
}

void AdvancedOpts::on_img_mthrSlider_sliderMoved(int position)
{
    QString s;
    s = s.setNum(position/10.0f,'f',1);
    ui->img_mthrLabel->setText( s );
}

void AdvancedOpts::on_img_resetBtn_clicked()
{
    ImgParams tmp;

    QString s;
    s = s.setNum(tmp.IMG_THRESHOLD,'f',1);
    ui->img_thrLabel->setText(s);
    ui->img_thrSlider->setValue(CASTINT(tmp.IMG_THRESHOLD*10));

    s = s.setNum(tmp.IMG_MED_THRESHOLD,'f',1);
    ui->img_mthrLabel->setText(s);
    ui->img_mthrSlider->setValue(CASTINT(tmp.IMG_MED_THRESHOLD*10));

    ui->img_sizeBox->setCurrentText(QString("%1").arg(tmp.IMG_SIZE));
    ui->img_blockszBox->setCurrentText(QString("%1").arg(tmp.IMG_BLOCK_SIZE));
}

void AdvancedOpts::on_vid_thrSlider_sliderMoved(int position)
{
    QString s;
    s = s.setNum(position/10.0f,'f',1);
    ui->vid_thrLabel->setText( s );
}

void AdvancedOpts::on_vid_diffSlider_sliderMoved(int position)
{
    ui->vid_diffLabel->setText( QString("%1%").arg(position) );
}

void AdvancedOpts::on_vid_resetBtn_clicked()
{
    VideoParams tmp;

    QString s;
    s = s.setNum(tmp.THRESHOLD,'f',1);
    ui->vid_thrLabel->setText(s);
    ui->vid_thrSlider->setValue(CASTINT(tmp.THRESHOLD*10));

    ui->vid_diffSlider->setValue(CASTINT(tmp.MAX_DURATION_DIFF*100));
    ui->vid_diffLabel->setText( QString("%1%").arg(CASTINT(tmp.MAX_DURATION_DIFF*100)) );

    ui->vid_sizeBox->setCurrentText(QString("%1").arg(tmp.IMG_SIZE));
}

void AdvancedOpts::on_aud_resetBtn_clicked()
{
    AudioParams tmp;

    QString s;
    s = s.setNum(tmp.THRESHOLD,'f',1);
    ui->aud_thrLabel->setText(s);
    ui->aud_thrSlider->setValue(CASTINT(tmp.THRESHOLD*10));

    ui->aud_diffSlider->setValue(CASTINT(tmp.MAX_DURATION_DIFF*100));
    ui->aud_diffLabel->setText( QString("%1%").arg(CASTINT(tmp.MAX_DURATION_DIFF*100)) );
}

void AdvancedOpts::on_aud_diffSlider_sliderMoved(int position)
{
    ui->aud_diffLabel->setText( QString("%1%").arg(position) );
}

void AdvancedOpts::on_aud_thrSlider_sliderMoved(int position)
{
    QString s;
    s = s.setNum(position/10.0f,'f',1);
    ui->aud_thrLabel->setText( s );
}
