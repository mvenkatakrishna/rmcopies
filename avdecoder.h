#ifndef AVDECODER_H
#define AVDECODER_H

#include <deque>

#include <QString>
#include <QFile>

#include <eigen/CXX11/Tensor>

#include "internaldefs.h"

extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libswresample/swresample.h>
#include <libswscale/swscale.h>
}

// Audio decoder class
class RCAudioDecoder
{
public:
    RCAudioDecoder( QString fName, AudioParams params );
    virtual ~RCAudioDecoder();

    bool decodeAudio(QVector<int16_t>& samples);
    AudioInfo getAudioInfo();

private:
    QString m_fName;
    AudioInfo m_info;
    int64_t m_in_sample_rate;

    AVFormatContext * m_formatCtx;
    AVCodec * m_codec;
    AVCodecContext * m_codecCtx;
    SwrContext * m_swr;
    AVFrame * m_frame;
    AudioParams m_params;
};

// Video decoder class
class RCVideoDecoder
{
public:
    RCVideoDecoder(QString fName, VideoParams params);
    virtual ~RCVideoDecoder();

    void skipNextFrame();
    void getNextFrame( Eigen::Tensor<uint8_t, 2>& img );
    VideoInfo getVideoInfo();

private:
    void m_decodeNextPacket();

    QString m_fName;
    VideoInfo m_info;

    AVFormatContext * m_formatCtx;
    AVCodec * m_codec;
    AVCodecContext * m_codecCtx;
    SwsContext * m_swsCtx;
    AVFrame * m_frame;
    VideoParams m_params;

    std::deque<Eigen::Tensor<uint8_t, 2>> m_frameBuf;
};

#endif // AVDECODER_H
